@ECHO OFF
chcp 850
setlocal enabledelayedexpansion
ECHO Author: Szczepkowski Marek
ECHO Date: 25-06-2019
ECHO Version: 1.0
ECHO.

SET QGIS_ROOT=C:\Program Files\QGIS 3.8
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET WORK=%cd%
SET COORDINATE=3006

REM COUNTER FILES
dir /b /s *.xyz 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1


FOR /F %%i IN ('dir /b/s "*.xyz" ') DO (
    ECHO.
	ECHO Processing  %%~ni    !Counter! / %count% FILES 
	
	gdal_translate -of GTiff -co "TFW=YES" %%i %%~pi%%~ni.tif
	
	set /A Counter+=1
	
)
ECHO.
ECHO Done
PAUSE